package listeners;

import controllers.student.smallLayout.AttemptedQuizListItemController;
import javafx.event.EventHandler;

public interface AttemptedQuizListItemClickListener extends EventHandler {
    public void itemClicked(Integer totalQuestions, Integer attemptedQuestions, AttemptedQuizListItemController controller);
}
