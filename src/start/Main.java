package start;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import models.*;
import org.sqlite.core.DB;
import util.DBConnect;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/views/Login.fxml"));

        primaryStage.setTitle("Quizzes");
        primaryStage.setScene(new Scene(root, 950, 650));

        primaryStage.show();
        createTables();
    }

    private void createTables(){
        Quiz.createTable();
        Question.createTable();
        Student.createTable();
        QuizResult.createTable();
        QuizResultDetails.createTable();
    }

    @Override
    public void stop() throws Exception {
        DBConnect.stop();
        System.exit(0);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
