package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnect {
    private static final String DB_NAME = "quiz.db";
    private static final String CONNECTION_STRING = "jdbc:sqlite:src/db/" + DB_NAME;

    private static Connection connection = null;

    public static Connection getConnection() throws SQLException
    {
        if(connection == null)
            connection = DriverManager.getConnection(CONNECTION_STRING);

        return connection;
    }

    public static void stop() throws SQLException{
        connection.close();
    }
}
