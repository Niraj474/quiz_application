package util;

import models.Question;
import models.Quiz;
import models.Student;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DataCollector{

    public static void readAndQuizzesData() throws IOException {

        String folderPath = "src/util/sample-data/quizzes";

        String basePath = "src/util/sample-data/quizzes/api (%d).json";

        File folder = new File(folderPath);

        String[] filenames = folder.list();

        for(String filename : filenames){
            File file = new File(folderPath + "/" + filename);

            FileReader reader = new FileReader(file);

            BufferedReader bufferedReader = new BufferedReader(reader);

            String line;
            StringBuilder stringBuilder = new StringBuilder();

            while((line = bufferedReader.readLine()) != null)
            {
//            System.out.println(line);
                stringBuilder.append(line);
            }

            JSONObject jsonObject = new JSONObject(stringBuilder.toString());

            JSONArray result = jsonObject.getJSONArray("results");

            Quiz quiz = new Quiz();

            List<Question> questions = new ArrayList<>();

            for(int i = 0; i < result.length(); ++i)
            {
                String objString = (String)result.get(i).toString();

                JSONObject obj = new JSONObject(objString);

                Question question = new Question();

                quiz.setTitle(obj.getString("category"));

                question.setQuestion(obj.getString("question"));

                JSONArray incorrectOptions = obj.getJSONArray("incorrect_answers");

                question.setOption1(incorrectOptions.get(0).toString());
                question.setOption2(incorrectOptions.get(1).toString());
                question.setOption3(incorrectOptions.get(2).toString());
                question.setOption4(obj.getString("correct_answer"));
                question.setAnswer(obj.getString("correct_answer"));

                questions.add(question);
                question.setQuiz(quiz);

            }
            quiz.save(questions);
        }


    }

    public static void readAndSaveUserData() throws IOException
    {
        // reading file
        File file = new File("src/util/sample-data/users.json");

        FileReader reader = new FileReader(file);

        BufferedReader bufferedReader = new BufferedReader(reader);

        String line;
        StringBuilder stringBuilder = new StringBuilder();

        while((line = bufferedReader.readLine()) != null)
            stringBuilder.append(line);

        // parsing json data
        JSONArray result = new JSONArray(stringBuilder.toString());

        for(int i = 0; i < result.length(); ++i)
        {
            String objString = result.get(i).toString();

            JSONObject obj = new JSONObject(objString);

            Student student = new Student();

            student.setFirstName(obj.getString("firstName"));
            student.setLastName(obj.getString("lastName"));
            student.setEmail(obj.getString("email"));
            Long pass = obj.getLong("password");
            student.setPassword(pass.toString().toCharArray());
            Long phone = obj.getLong("phone");
            student.setContact(phone.toString());
            student.setGender('M');
            student.save();

        }
    }

    public static void main(String[] args) {
        try{
//            Quiz.createTable();
//            Question.createTable();
//            readAndQuizzesData();

            Student.createTable();
            readAndSaveUserData();

        }catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}