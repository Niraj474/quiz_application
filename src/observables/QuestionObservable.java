package observables;

import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import models.Question;

public class QuestionObservable {

    // This class is used for binding the questions and options of GUI with the question object.
    public Property<String> questionProperty() {
        return question;
    }

    public Property<String> option1Property() {
        return option1;
    }

    public Property<String> option2Property() {
        return option2;
    }


    public Property<String> option3Property() {
        return option3;
    }


    public Property<String> option4Property() {
        return option4;
    }

    private Property<String> question = new SimpleStringProperty();
    private Property<String> option1 = new SimpleStringProperty();
    private Property<String> option2 = new SimpleStringProperty();
    private Property<String> option3 = new SimpleStringProperty();
    private Property<String> option4 = new SimpleStringProperty();

    public void setQuestion(Question question)
    {
        this.question.setValue(question.getQuestion());
        this.option1.setValue(question.getOption1());
        this.option2.setValue(question.getOption2());
        this.option3.setValue(question.getOption3());
        this.option4.setValue(question.getOption4());
    }
}
