package models;

import util.DBConnect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.Set;

public class QuizResultDetails {

    public static class MetaData{
        public static final String TABLE_NAME = "quiz_result_details";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_QUIZ_RESULT_ID = "quiz_result_id";
        public static final String COLUMN_QUESTION_ID = "question_id";
        public static final String COLUMN_USER_ANSWERED = "user_answered";
    }

    public static void createTable()
    {
        String raw = "CREATE TABLE IF NOT EXISTS %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER NOT NULL," +
                " %s INTEGER NOT NULL, %s VARCHAR(200) NOT NULL, " +
                "FOREIGN KEY (%s) REFERENCES %s(%s), " +
                "FOREIGN KEY (%s) REFERENCES %s(%s) )";

        String query = String.format(raw, MetaData.TABLE_NAME, MetaData.COLUMN_ID, MetaData.COLUMN_QUIZ_RESULT_ID,
                MetaData.COLUMN_QUESTION_ID, MetaData.COLUMN_USER_ANSWERED,
                MetaData.COLUMN_QUIZ_RESULT_ID, QuizResult.MetaData.TABLE_NAME,QuizResult.MetaData.COLUMN_ID,
                MetaData.COLUMN_QUESTION_ID, Question.MetaData.TABLE_NAME, Question.MetaData.COLUMN_ID);

//        System.out.println(query);

        try {

            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            boolean b = preparedStatement.execute();

            preparedStatement.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public static boolean save(QuizResult quizResult, Map<Question,String> userAnswers) {
        String raw = "INSERT INTO %s (%s,%s,%s) VALUES (?,?,?)";

        String query = String.format(raw, MetaData.TABLE_NAME, MetaData.COLUMN_QUIZ_RESULT_ID, MetaData.COLUMN_QUESTION_ID, MetaData.COLUMN_USER_ANSWERED);

        try {

            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            Set<Question> keys = userAnswers.keySet();

            for(Question question : keys)
            {
                preparedStatement.setInt(1, quizResult.getId());
                preparedStatement.setInt(2, question.getQuestionId());
                preparedStatement.setString(3, userAnswers.get(question));
                preparedStatement.addBatch();
            }

            int[] result = preparedStatement.executeBatch();

            if(result.length > 0)
                return true;

        }catch (SQLException e)
        {
            e.printStackTrace();
            return false;
        }
        return false;
    }
}
