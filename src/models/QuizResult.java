package models;

import util.DBConnect;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

public class QuizResult {

    private Integer id;
    private Integer rightAnswersCount;
    private Quiz quiz;
    private Student student;

    public QuizResult() {

    }

    public static class MetaData{
        public static final String TABLE_NAME = "quiz_results";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_RIGHT_ANSWERS = "right_answers";
        public static final String COLUMN_DATE_TIME = "date_time";
        public static final String COLUMN_QUIZ_ID = "quiz_id";
        public static final String COLUMN_STUDENT_ID = "student_id";
    }

    public QuizResult(Integer rightAnswersCount, Quiz quiz, Student student) {
        this.rightAnswersCount = rightAnswersCount;
        this.quiz = quiz;
        this.student = student;
    }

    public QuizResult(Integer id, Integer rightAnswersCount, Quiz quiz, Student student) {
        this.id = id;
        this.rightAnswersCount = rightAnswersCount;
        this.quiz = quiz;
        this.student = student;
    }

    public static void createTable()
    {
        String raw = "CREATE TABLE IF NOT EXISTS %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s INTEGER NOT NULL," +
                " %s INTEGER NOT NULL, %s INTEGER NOT NULL, %s TIMESTAMP, " +
                "FOREIGN KEY (%s) REFERENCES %s(%s), " +
                "FOREIGN KEY (%s) REFERENCES %s(%s) )";

        String query = String.format(raw, MetaData.TABLE_NAME, MetaData.COLUMN_ID, MetaData.COLUMN_QUIZ_ID,
                MetaData.COLUMN_STUDENT_ID, MetaData.COLUMN_RIGHT_ANSWERS, MetaData.COLUMN_DATE_TIME,
                MetaData.COLUMN_QUIZ_ID, Quiz.MetaData.TABLE_NAME,Quiz.MetaData.COLUMN_ID,
                MetaData.COLUMN_STUDENT_ID, Student.MetaData.TABLE_NAME, Student.MetaData.COLUMN_ID);

//        System.out.println(query);

        try {

            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            boolean b = preparedStatement.execute();

            preparedStatement.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Integer getId() {
        return id;
    }

    public Integer getRightAnswersCount() {
        return rightAnswersCount;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public Student getStudent() {
        return student;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setRightAnswersCount(Integer rightAnswersCount) {
        this.rightAnswersCount = rightAnswersCount;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public boolean save(Map<Question,String> userAnswers)
    {
        String raw = "INSERT INTO %s (%s,%s,%s,%s) VALUES (?,?,?,CURRENT_TIMESTAMP)";

        String query = String.format(raw,MetaData.TABLE_NAME,MetaData.COLUMN_QUIZ_ID,MetaData.COLUMN_STUDENT_ID,
                MetaData.COLUMN_RIGHT_ANSWERS,MetaData.COLUMN_DATE_TIME);

        try {

            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, this.quiz.getQuizId());
            preparedStatement.setInt(2, this.student.getId());
            preparedStatement.setInt(3, this.rightAnswersCount);


            int result = preparedStatement.executeUpdate();

            if(result > 0) {
                ResultSet keys = preparedStatement.getGeneratedKeys();
                if(keys.next())
                {
                    this.setId(keys.getInt(1));
                    return this.saveQuizResultDetails(userAnswers);
                }
            }

            preparedStatement.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
        return false;
    }

    public static Map<QuizResult,Quiz> getQuizzes(Student student)
    {
        Map<QuizResult,Quiz> data = new HashMap<>();

        String raw = "SELECT %s.%s as quiz_id, %s.%s, %s, %s.%s FROM %s JOIN %s on %s.%s = %s.%s WHERE %s = ?";

        String query = String.format(raw, Quiz.MetaData.TABLE_NAME,Quiz.MetaData.COLUMN_ID,Quiz.MetaData.TABLE_NAME,Quiz.MetaData.COLUMN_TITLE,
                MetaData.COLUMN_RIGHT_ANSWERS, MetaData.TABLE_NAME, MetaData.COLUMN_ID,MetaData.TABLE_NAME,Quiz.MetaData.TABLE_NAME,
                MetaData.TABLE_NAME,MetaData.COLUMN_QUIZ_ID,Quiz.MetaData.TABLE_NAME,Quiz.MetaData.COLUMN_ID,MetaData.COLUMN_STUDENT_ID);

        try {

            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1,student.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next())
            {
                QuizResult quizResult = new QuizResult();
                quizResult.setId(resultSet.getInt(4));
                quizResult.setRightAnswersCount(resultSet.getInt(3));

                Quiz quiz = new Quiz();
                quiz.setQuizId(resultSet.getInt(1));
                quiz.setTitle(resultSet.getString(2));

                data.put(quizResult,quiz);
            }

        }
        catch (SQLException throwables) {
            throwables.printStackTrace();

        }

        return data;
    }

    public Integer getNumberOfAttemptedQuestions()
    {
        String raw = "select count(*) from %s where %s.%s = ? and %s.%s <> \"\"";

        String query = String.format(raw,QuizResultDetails.MetaData.TABLE_NAME,QuizResultDetails.MetaData.TABLE_NAME,
                QuizResultDetails.MetaData.COLUMN_QUIZ_RESULT_ID, QuizResultDetails.MetaData.TABLE_NAME,QuizResultDetails.MetaData.COLUMN_USER_ANSWERED);


        try
        {
            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1,this.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next())
                return resultSet.getInt(1);

            preparedStatement.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return 0;
    }

    public Map<Question,String> getQuizResultDetails() {
        Map<Question,String> userAnswers = new HashMap<>();

        String raw = "select %s,%s,%s,%s,%s,%s,%s,%s.%s from %s join %s ON %s.%s = %s.%s where %s.%s = ? and %s.%s <> \"\" ";

        String query = String.format(raw,QuizResultDetails.MetaData.COLUMN_QUESTION_ID, Question.MetaData.COLUMN_QUESTION,
                Question.MetaData.COLUMN_OPTION1, Question.MetaData.COLUMN_OPTION2, Question.MetaData.COLUMN_OPTION3,
                Question.MetaData.COLUMN_OPTION4, Question.MetaData.COLUMN_ANSWER, QuizResultDetails.MetaData.TABLE_NAME,
                QuizResultDetails.MetaData.COLUMN_USER_ANSWERED, Question.MetaData.TABLE_NAME,
                QuizResultDetails.MetaData.TABLE_NAME, Question.MetaData.TABLE_NAME, Question.MetaData.COLUMN_ID,
                QuizResultDetails.MetaData.TABLE_NAME, QuizResultDetails.MetaData.COLUMN_QUESTION_ID,
                QuizResultDetails.MetaData.TABLE_NAME, QuizResultDetails.MetaData.COLUMN_QUIZ_RESULT_ID,
                QuizResultDetails.MetaData.TABLE_NAME,QuizResultDetails.MetaData.COLUMN_USER_ANSWERED);

        System.out.println(query);

        try {
            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1, this.getId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next())
            {
                Question question = new Question();
                question.setQuestionId(resultSet.getInt(1));
                question.setQuestion(resultSet.getString(2));
                question.setOption1(resultSet.getString(3));
                question.setOption2(resultSet.getString(4));
                question.setOption3(resultSet.getString(5));
                question.setOption4(resultSet.getString(6));
                question.setAnswer(resultSet.getString(7));


                String userAnswer = resultSet.getString(8);

                userAnswers.put(question,userAnswer);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return userAnswers;

    }

    private boolean saveQuizResultDetails(Map<Question,String> userAnswers)
    {
        return QuizResultDetails.save(this,userAnswers);
    }
}
