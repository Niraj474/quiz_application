package models;

import util.DBConnect;

import java.io.IOException;
import java.sql.*;
import java.util.*;

public class Quiz {

    private Integer quizId;
    private String title;

    // constants for db
    public static class MetaData {
        public static final String TABLE_NAME = "quizzes";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_TITLE = "title";
    }


    public Quiz(String title) {
        this.title = title;
    }

    public Quiz() {
    }

    public Integer getQuizId() {
        return quizId;
    }

    public void setQuizId(Integer quizId) {
        this.quizId = quizId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return this.title;
    }

    public static void createTable(){
        String rawQuery = "CREATE TABLE IF NOT EXISTS %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT, %s VARCHAR(50))";

        String query = String.format(rawQuery, MetaData.TABLE_NAME, MetaData.COLUMN_ID,MetaData.COLUMN_TITLE);


        try {

            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            boolean b = preparedStatement.execute();

            preparedStatement.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public int save(){
        String rawQuery = "INSERT INTO %s (%s) VALUES (?)";

        String query = String.format(rawQuery, MetaData.TABLE_NAME, MetaData.COLUMN_TITLE);


        try {

            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, this.title);
            int i = preparedStatement.executeUpdate();
            ResultSet result = preparedStatement.getGeneratedKeys();

            if(result.next())
                return result.getInt(1);

            result.close();
            preparedStatement.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return -1;
        }
        return -1;
    }

    public boolean save(List<Question> questions){
        boolean flag = true;

        this.quizId = save();

        for(Question question : questions)
            flag = flag && question.save();

        return flag;
    }

    @Override
    public int hashCode() {
        return Objects.hash(quizId, title); // Here We're calculating hashCode based on the quizId and title bcoz if we hash the object based on only id then there might be the case where two different id's generate same hash value so we need some another unique identifier also to make hash function stronger.
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null || (!(obj instanceof Quiz)))
            return false;

        Quiz quiz = (Quiz) obj;

        if(this.quizId == quiz.quizId)
            return true;

        return false;
    }

    public static Map<Quiz, List<Question>> getAll()
    {
        Map<Quiz, List<Question>> quizzes = new HashMap<>();
        Quiz key = null;

        String raw = "SELECT %s.%s as quizId, %s, %s.%s, %s, %s, %s, %s, %s, %s FROM %s join %s on quizId = %s.%s";

        String query = String.format(raw, MetaData.TABLE_NAME,MetaData.COLUMN_ID, MetaData.COLUMN_TITLE,
                Question.MetaData.TABLE_NAME, Question.MetaData.COLUMN_ID, Question.MetaData.COLUMN_QUESTION,
                Question.MetaData.COLUMN_OPTION1, Question.MetaData.COLUMN_OPTION2, Question.MetaData.COLUMN_OPTION3,
                Question.MetaData.COLUMN_OPTION4, Question.MetaData.COLUMN_ANSWER, MetaData.TABLE_NAME,
                Question.MetaData.TABLE_NAME,Question.MetaData.TABLE_NAME,Question.MetaData.COLUMN_QUIZ_ID);

//        System.out.println(query);
        try {

            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet results = preparedStatement.executeQuery();

            while(results.next())
            {
                Quiz temp = new Quiz();
                temp.setQuizId(results.getInt(1));
                temp.setTitle(results.getString(2));

                Question question = new Question();
                question.setQuestionId(results.getInt(3));
                question.setQuestion(results.getString(4));
                question.setOption1(results.getString(5));
                question.setOption2(results.getString(6));
                question.setOption3(results.getString(7));
                question.setOption4(results.getString(8));
                question.setAnswer(results.getString(9));
                question.setQuiz(temp);

                if(key != null && key.equals(temp))
                    quizzes.get(key).add(question);
                else{
                    List<Question> questions = new LinkedList<>();
                    questions.add(question);
                    quizzes.put(temp,questions);
                }

                key = temp;
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return quizzes;
    }


    public static Map<Quiz,Integer> getAllWithQuestionCount()
    {
        Map<Quiz, Integer> quizzes = new HashMap<>();
        Quiz key = null;

        String raw = "SELECT %s.%s as quizId, %s, COUNT(*) AS question_count FROM %s " +
                "join %s on quizId = %s.%s GROUP BY quizId";

        String query = String.format(raw, MetaData.TABLE_NAME,MetaData.COLUMN_ID, MetaData.COLUMN_TITLE, MetaData.TABLE_NAME,
                Question.MetaData.TABLE_NAME,Question.MetaData.TABLE_NAME,Question.MetaData.COLUMN_QUIZ_ID);

//        System.out.println(query);
        try {

            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet results = preparedStatement.executeQuery();

            while(results.next())
            {
                Quiz temp = new Quiz();
                temp.setQuizId(results.getInt(1));
                temp.setTitle(results.getString(2));

                int count = results.getInt(3);

                quizzes.put(temp,count);
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return quizzes;
    }

    public List<Question> getQuestions() {
        List<Question> questions = new ArrayList<>();
        String raw = "SELECT %s, %s, %s, %s, %s, %s, %s FROM %s WHERE %s = ?";

        String query = String.format(raw, Question.MetaData.COLUMN_ID, Question.MetaData.COLUMN_QUESTION,
                Question.MetaData.COLUMN_OPTION1, Question.MetaData.COLUMN_OPTION2, Question.MetaData.COLUMN_OPTION3,
                Question.MetaData.COLUMN_OPTION4, Question.MetaData.COLUMN_ANSWER, Question.MetaData.TABLE_NAME,
                Question.MetaData.COLUMN_QUIZ_ID);

        try {

            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1, this.getQuizId());
            ResultSet results = preparedStatement.executeQuery();

            while (results.next()) {
                Question question = new Question();
                question.setQuestionId(results.getInt(1));
                question.setQuestion(results.getString(2));
                question.setOption1(results.getString(3));
                question.setOption2(results.getString(4));
                question.setOption3(results.getString(5));
                question.setOption4(results.getString(6));
                question.setAnswer(results.getString(7));
                question.setQuiz(this);
                questions.add(question);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return questions;
    }

    public Integer getNumberOfQuestions()
    {
        String raw = "SELECT COUNT(*) FROM %s WHERE %s = ?";

        String query = String.format(raw, Question.MetaData.TABLE_NAME,Question.MetaData.COLUMN_QUIZ_ID);

//        System.out.println(query);

        try
        {
            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setInt(1,this.getQuizId());

            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next())
                return resultSet.getInt(1);

            preparedStatement.close();
        }
        catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return 0;
    }


}
