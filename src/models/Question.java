package models;

import util.DBConnect;

import java.sql.*;
import java.util.List;

public class Question {
    Quiz quiz;
    Integer questionId;
    String question;
    String option1;
    String option2;
    String option3;
    String option4;
    String answer;

    // constants for db
    public static class MetaData{
        public static final String TABLE_NAME = "questions";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_QUESTION = "question";
        public static final String COLUMN_QUIZ_ID = "quiz_id";
        public static final String COLUMN_OPTION1 = "option1";
        public static final String COLUMN_OPTION2 = "option2";
        public static final String COLUMN_OPTION3 = "option3";
        public static final String COLUMN_OPTION4 = "option4";
        public static final String COLUMN_ANSWER = "answer";
    }


    public Question(Quiz quiz, String question, String option1, String option2, String option3, String option4, String answer) {
        this.quiz = quiz;
        this.question = question;
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
        this.answer = answer;
    }

    public Question() {
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    public Integer getQuestionId() {
        return questionId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOption4() {
        return option4;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return this.question;
    }

    public void setQuestionId(Integer questionId) {
        this.questionId = questionId;
    }

    public static void createTable(){
        try {

            Connection connection = DBConnect.getConnection();

            String rawQuery = "CREATE TABLE IF NOT EXISTS %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT, %s varchar(1000), %s varchar(500), %s varchar(500), %s varchar(500), %s varchar(500), %s varchar(500), %s integer, foreign key (%s) references %s(%s))";

            String query = String.format(rawQuery,
                                        MetaData.TABLE_NAME,
                                        MetaData.COLUMN_ID,
                                        MetaData.COLUMN_QUESTION,
                                        MetaData.COLUMN_OPTION1,
                                        MetaData.COLUMN_OPTION2,
                                        MetaData.COLUMN_OPTION3,
                                        MetaData.COLUMN_OPTION4,
                                        MetaData.COLUMN_ANSWER,
                                        MetaData.COLUMN_QUIZ_ID,
                                        MetaData.COLUMN_QUIZ_ID,
                                        Quiz.MetaData.TABLE_NAME,
                                        Quiz.MetaData.COLUMN_ID);

            

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.execute();

            preparedStatement.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public boolean save(){
        boolean flag = false;
        String rawQuery = "INSERT INTO %s (%s,%s,%s,%s,%s,%s,%s) VALUES (?,?,?,?,?,?,?)";

        String query = String.format(rawQuery, MetaData.TABLE_NAME, MetaData.COLUMN_QUESTION,
                                    MetaData.COLUMN_OPTION1,MetaData.COLUMN_OPTION2,MetaData.COLUMN_OPTION3,
                                    MetaData.COLUMN_OPTION4,MetaData.COLUMN_ANSWER,MetaData.COLUMN_QUIZ_ID);



        try {

            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setString(1, this.question);
            preparedStatement.setString(2, this.option1);
            preparedStatement.setString(3, this.option2);
            preparedStatement.setString(4, this.option3);
            preparedStatement.setString(5, this.option4);
            preparedStatement.setString(6, this.answer);
            preparedStatement.setInt(7, this.quiz.getQuizId());

            int i = preparedStatement.executeUpdate();
            System.out.println("Updated Rows : " + i);

            flag = true;

            preparedStatement.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }

        return flag;
    }



}
