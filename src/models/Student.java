package models;

import exceptions.LoginException;
import util.DBConnect;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Student {

    private Integer id;
    private String firstName;
    private String lastName;

    private String contact;

    private Character gender;
    private String email;

    private char[] password;

    // constants for db
    public static class MetaData {
        public static final String TABLE_NAME = "students";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_FIRST_NAME = "first_name";
        public static final String COLUMN_LAST_NAME = "last_name";
        public static final String COLUMN_CONTACT = "contact_no";
        public static final String COLUMN_GENDER = "gender";
        public static final String COLUMN_EMAIL = "email";
        public static final String COLUMN_PASSWORD = "password";
    }

    public Student()
    {

    }

    public Student(String firstName, String lastName, String contact, Character gender, String email, char[] password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.contact = contact;
        this.gender = gender;
        this.email = email;
        this.password = password;
    }

    public Student(Integer id, String firstName, String lastName, String contact, Character gender, String email, char[] password) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.contact = contact;
        this.gender = gender;
        this.email = email;
        this.password = password;
    }
    public Student(String email, String password)
    {
        this.email = email;
        this.password = password.toCharArray();
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public void setGender(Character gender) {
        this.gender = gender;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(char[] password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getContact() {
        return contact;
    }

    public Character getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        StringBuffer sb = new StringBuffer();

        for(char ch : this.password)
            sb.append(ch);
        return sb.toString();
    }

    public static void createTable() {
        String raw = "CREATE TABLE IF NOT EXISTS %s " +
                "( %s INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "%s VARCHAR(20), %s VARCHAR(20),%s VARCHAR(20),%s VARCHAR(20),%s VARCHAR(20)," +
                "%s char )";

        String query = String.format(raw,
                MetaData.TABLE_NAME,
                MetaData.COLUMN_ID,
                MetaData.COLUMN_FIRST_NAME,
                MetaData.COLUMN_LAST_NAME,
                MetaData.COLUMN_CONTACT,
                MetaData.COLUMN_EMAIL,
                MetaData.COLUMN_PASSWORD,
                MetaData.COLUMN_GENDER);

        try  {
            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            int i = preparedStatement.executeUpdate();
            System.out.println(i);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Student save()
    {
        String raw = "INSERT INTO %s (%s, %s, %s, %s, %s, %s) VALUES (?,?,?,?,?,?)";

        String query = String.format(raw,MetaData.TABLE_NAME, MetaData.COLUMN_FIRST_NAME,MetaData.COLUMN_LAST_NAME, MetaData.COLUMN_CONTACT, MetaData.COLUMN_EMAIL, MetaData.COLUMN_PASSWORD, MetaData.COLUMN_GENDER);

        try {

            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, this.firstName);
            preparedStatement.setString(2, this.lastName);
            preparedStatement.setString(3, this.contact);
            preparedStatement.setString(4, this.email);
            preparedStatement.setString(5, String.valueOf(this.password));
            preparedStatement.setString(6, String.valueOf(this.gender));

            int i = preparedStatement.executeUpdate();
            ResultSet result = preparedStatement.getGeneratedKeys();

            if(result.next())
                this.id  = result.getInt(1);

            result.close();
            preparedStatement.close();

            return this;

        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", contact='" + contact + '\'' +
                ", gender=" + gender +
                ", email='" + email + '\'' +
                ", password=" + Arrays.toString(password) +
                '}';
    }

    public static List<Student> getAll()
    {
        List<Student> students = new ArrayList<>();

        String raw = "SELECT %s, %s, %s, %s, %s, %s, %s FROM %s";

        String query = String.format(raw, MetaData.COLUMN_ID,
                MetaData.COLUMN_FIRST_NAME, MetaData.COLUMN_LAST_NAME,
                MetaData.COLUMN_CONTACT, MetaData.COLUMN_EMAIL,
                MetaData.COLUMN_PASSWORD, MetaData.COLUMN_GENDER, MetaData.TABLE_NAME);

        try {
            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet results = preparedStatement.executeQuery();

            while (results.next())
            {
                Student student = new Student();
                student.setId(results.getInt(1));
                student.setFirstName(results.getString(2));
                student.setLastName(results.getString(3));
                student.setContact(results.getString(4));
                student.setEmail(results.getString(5));
                student.setPassword(results.getString(6).toCharArray());
                student.setGender(results.getString(7).charAt(0));

                students.add(student);
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return students;
    }

    public boolean isExisting()
    {
        String raw = "SELECT * from %s where %s = ?";

        String query = String.format(raw,MetaData.TABLE_NAME,MetaData.COLUMN_EMAIL);

        try{
            Connection connection = DBConnect.getConnection();

            PreparedStatement preparedStatement = connection.prepareStatement(query);

            preparedStatement.setString(1,this.email);

            ResultSet result = preparedStatement.executeQuery();

            if(result.next())
                return true;

            connection.close();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }

    public void login() throws SQLException,LoginException
    {
        String raw = "SELECT %s, %s, %s, %s, %s from %s where %s = ? and %s = ?";

        String query = String.format(raw,MetaData.COLUMN_ID,MetaData.COLUMN_FIRST_NAME,
                MetaData.COLUMN_LAST_NAME, MetaData.COLUMN_CONTACT, MetaData.COLUMN_GENDER,
                MetaData.TABLE_NAME, MetaData.COLUMN_EMAIL,MetaData.COLUMN_PASSWORD);

        Connection connection = DBConnect.getConnection();

        PreparedStatement preparedStatement = connection.prepareStatement(query);

        preparedStatement.setString(1,this.email);

        preparedStatement.setString(2, this.getPassword());

        ResultSet result = preparedStatement.executeQuery();

        if(result.next())
        {
            this.setId(result.getInt(1));
            this.setFirstName(result.getString(2));
            this.setLastName(result.getString(3));
            this.setContact(result.getString(4));
            this.setGender(result.getString(5).charAt(0));

            preparedStatement.close();
        }
        else
            throw new LoginException("Invalid Email or Password");
    }

}
