package controllers;

import constants.AdminEmailPassword;
import controllers.student.AllQuizzesController;
import controllers.student.MainScreenController;
import exceptions.LoginException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;
import models.Student;
import org.controlsfx.control.Notifications;

import java.io.IOException;

public class LoginController {

    @FXML
    private TextField adminEmailTxt;
    @FXML
    private PasswordField adminPasswordTxt;
    @FXML
    private Button loginAdminBtn;
    @FXML
    private Button loginStudentBtn;
    @FXML
    private TextField studentEmailTxt;
    @FXML
    private PasswordField studentPasswordTxt;


    public void loginAdmin(){
        String email = adminEmailTxt.getText();
        String password = adminPasswordTxt.getText();

        if(email.trim().equals(AdminEmailPassword.email) && password.trim().equals((AdminEmailPassword.password)))
        {
            Parent root = null;

            try {
                root = FXMLLoader.load(getClass().getResource("/views/admin/AdminHomePage.fxml"));
            } catch (IOException e) {
                e.printStackTrace();
            }

            Stage stage = (Stage) studentPasswordTxt.getScene().getWindow();
            Scene scene = new Scene(root);
            stage.setScene(scene);
            stage.setMaximized(true);
        }
        else
            System.out.println("Admin Login Failed");
    }

    public void loginStudent(){
        String email = studentEmailTxt.getText().trim();
        String password = studentPasswordTxt.getText().trim();

        Student student = new Student(email,password);
        try
        {
            student.login();
            try{
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/student/MainScreen.fxml"));
                Parent root = loader.load();
                MainScreenController controller = loader.getController();
                controller.setStudent(student);
                Stage stage = (Stage) studentPasswordTxt.getScene().getWindow();

                Scene scene = new Scene(root);

                stage.setScene(scene);

                stage.setWidth(950);
                stage.setHeight(650);


            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }catch (Exception e)
        {
            if(e instanceof LoginException)
                Notifications.create().text(e.getMessage()).title("Invalid Email or Password").position(Pos.CENTER).showError();
            else
                e.printStackTrace();
        }
    }
}
