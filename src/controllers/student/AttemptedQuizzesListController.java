package controllers.student;

import com.jfoenix.controls.JFXButton;
import controllers.student.smallLayout.AttemptedQuizListItemController;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import listeners.AttemptedQuizListItemClickListener;
import models.Question;
import models.Quiz;
import models.QuizResult;
import models.Student;


import java.net.URL;
import java.util.*;

public class AttemptedQuizzesListController implements Initializable {

    @FXML
    private VBox quizzesList;
    @FXML
    private Label totalQuestions;
    @FXML
    private Label attemptedQuestions;
    @FXML
    private Label correctAnswers;
    @FXML
    private Label incorrectAnswers;
    @FXML
    private VBox resultDetailsContainer;
    @FXML
    private PieChart attemptedQuestionsChart;
    @FXML
    private PieChart correctAnswersChart;
    @FXML
    private JFXButton showDetailsBtn;
    @FXML
    private StackPane detailsStack;


    private Student student;

    private AttemptedQuizListItemController currentlyClickedItem;

    public void setStudent(Student student) {
        this.student = student;
        setAttemptedQuizzesList();
    }

    private void setFieldsOnItemClicked(Integer totalQuestions,Integer attemptedQuestions,Integer correctAnswersCount,Integer incorrectAnswersCount)
    {
        this.totalQuestions.setText(totalQuestions.toString());
        this.attemptedQuestions.setText(attemptedQuestions.toString());
        correctAnswers.setText(correctAnswersCount.toString());
        incorrectAnswers.setText(incorrectAnswersCount.toString());
    }

    private void setChartsOnItemClicked(Integer totalQuestions,Integer attemptedQuestions,Integer correctAnswersCount,Integer incorrectAnswersCount)
    {
        if(this.attemptedQuestionsChart == null && this.correctAnswersChart == null)
        {
            this.attemptedQuestionsChart = new PieChart();
            this.correctAnswersChart = new PieChart();

        }else
        {
            this.attemptedQuestionsChart.getData().clear();
            this.correctAnswersChart.getData().clear();
        }

        this.attemptedQuestionsChart.getData().add(new PieChart.Data("Attempted (" + attemptedQuestions + ")",attemptedQuestions));
        this.attemptedQuestionsChart.getData().add(new PieChart.Data("Not Attempted (" + (totalQuestions - attemptedQuestions) + ")",(totalQuestions - attemptedQuestions)));

        this.correctAnswersChart.getData().add(new PieChart.Data("Correct Answers " + correctAnswersCount,correctAnswersCount));
        this.correctAnswersChart.getData().add(new PieChart.Data("InCorrect Answers " + incorrectAnswersCount,incorrectAnswersCount));

    }

    private void renderQuizDetailsContainer()
    {

    }

    private void setAttemptedQuizzesList()
    {
        Map<QuizResult, Quiz> data = QuizResult.getQuizzes(this.student);

        Set<QuizResult> keys = data.keySet();

        for(QuizResult quizResult : keys)
        {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/student/smallLayout/AttemptedQuizListItem.fxml"));
                Parent root = loader.load();
                AttemptedQuizListItemController controller = loader.getController();
                controller.setData(data.get(quizResult),quizResult);
                controller.setClickListener(new AttemptedQuizListItemClickListener() {
                    @Override
                    public void itemClicked(Integer totalNumberOfQuestions,Integer attemptedQuestions,AttemptedQuizListItemController controller) {

                        Integer correctAnswers = quizResult.getRightAnswersCount();
                        Integer incorrectAnswers = attemptedQuestions - correctAnswers;

                        if(!resultDetailsContainer.isVisible())
                            resultDetailsContainer.setVisible(true);

                        setFieldsOnItemClicked(totalNumberOfQuestions,attemptedQuestions,correctAnswers,incorrectAnswers);

                        setChartsOnItemClicked(totalNumberOfQuestions,attemptedQuestions,correctAnswers,incorrectAnswers);

                        if(currentlyClickedItem != null)
                            currentlyClickedItem.removeBorder();
                        controller.setBorder();
                        currentlyClickedItem = controller;

                        if(detailsStack.getChildren().size() > 1)
                            detailsStack.getChildren().remove(detailsStack.getChildren().size() - 1);

                        renderQuizDetailsContainer();
                    }

                    @Override
                    public void handle(Event event) {

                    }
                });
                this.quizzesList.getChildren().add(root);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void fetchDetails()
    {
        try{

            Map<Question,String> userAnswers = this.currentlyClickedItem.getQuizResult().getQuizResultDetails();
            List<Question> questionList =  this.currentlyClickedItem.getQuiz().getQuestions();


            Quiz quiz = this.currentlyClickedItem.getQuiz();
            QuizResult quizResult = this.currentlyClickedItem.getQuizResult();


            ArrayList<Node> nodes = new ArrayList<>();

            VBox wrapper = new VBox();
            wrapper.setSpacing(10);
            ScrollPane scrollPane = new ScrollPane();

            for(Question question : userAnswers.keySet())
            {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/student/QuestionResult.fxml"));
                Parent node = loader.load();;
                QuestionResultController controller = loader.getController();
                controller.setValues(question,userAnswers.get(question));
                nodes.add(node);
            }

            wrapper.getChildren().addAll(nodes);
            wrapper.setFillWidth(true);
            scrollPane.setContent(wrapper);
            scrollPane.setFitToHeight(true);
            scrollPane.setFitToWidth(true);
            detailsStack.getChildren().add(scrollPane);


        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        resultDetailsContainer.setVisible(false);
    }
}
