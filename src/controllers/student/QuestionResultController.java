package controllers.student;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import models.Question;

import java.net.URL;
import java.util.ResourceBundle;

public class QuestionResultController implements Initializable {

    @FXML
    private Label question;
    @FXML
    private Label option1;
    @FXML
    private Label option2;
    @FXML
    private Label option3;
    @FXML
    private Label option4;

    private Question currQuestion;
    private String selectedAnswer;

    public void setValues(Question currQuestion, String selectedAnswer) {
        this.currQuestion = currQuestion;
        this.selectedAnswer = selectedAnswer;
        setData();
    }

    private void setData()
    {
        this.question.setText(this.currQuestion.getQuestion());
        this.option1.setText(this.currQuestion.getOption1());
        this.option2.setText(this.currQuestion.getOption2());
        this.option3.setText(this.currQuestion.getOption3());
        this.option4.setText(this.currQuestion.getOption4());

        setTextFillForOptions();
    }

    private void setTextFillForOptions()
    {
        Label selectedAns,correctAns;

        if(!this.selectedAnswer.equals(""))
        {
            if(this.option1.getText().equals(selectedAnswer))
                selectedAns = this.option1;
            else if(this.option2.getText().equals(selectedAnswer))
                selectedAns = this.option2;
            else if(this.option3.getText().equals(selectedAnswer))
                selectedAns = this.option3;
            else
                selectedAns = this.option4;

            selectedAns.setTextFill(Color.web("#E21717"));
        }

        {
            //Setting color for correct option

            if(this.option1.getText().equals(currQuestion.getAnswer()))
                correctAns = this.option1;
            else if(this.option2.getText().equals(currQuestion.getAnswer()))
                correctAns = this.option2;
            else if(this.option3.getText().equals(currQuestion.getAnswer()))
                correctAns = this.option3;
            else
                correctAns = this.option4;

            correctAns.setTextFill(Color.web("#4DD637"));
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }


}
