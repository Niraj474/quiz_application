package controllers.student;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.chart.PieChart;
import javafx.scene.layout.VBox;
import models.Question;
import models.Quiz;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

public class QuizResultController implements Initializable {

    @FXML
    private PieChart attemptedQuestionsChart;
    @FXML
    private PieChart correctAnswersChart;
    @FXML
    private VBox questionResultsContainer;

    //Non fxml variables
    Map<Question,String> userAnswers;
    Integer numberOfCorrectAnswers = 0;
    Integer numberOfIncorrectAnswers = 0;
    Quiz quiz;
    List<Question> questionList;
    Integer attemptedQuestions = 0;
    Integer notAttemptedQuestions = 0;


    public void setValues(Map<Question, String> userAnswers, Integer numberOfRightAnswers, Quiz quiz, List<Question> questionList) {
        this.userAnswers = userAnswers;
        this.numberOfCorrectAnswers = numberOfRightAnswers;
        this.quiz = quiz;
        this.questionList = questionList;
        setNumOfAttemptedAndNotAttemptedQuestions();
        this.numberOfIncorrectAnswers = this.attemptedQuestions - this.numberOfCorrectAnswers;
        setValuesToChart();
        renderQuestions();
    }

    private void setNumOfAttemptedAndNotAttemptedQuestions()
    {
        Set<Question> keys = this.userAnswers.keySet();

        for(Question question : keys)
        {
            if(!userAnswers.get(question).equals(""))
                ++attemptedQuestions;
            else
                ++notAttemptedQuestions;
        }
    }

    private void setValuesToChart()
    {
        ObservableList<PieChart.Data> attemptedData = this.attemptedQuestionsChart.getData();
        attemptedData.add(new PieChart.Data(String.format("Attempted (%d)", this.attemptedQuestions),this.attemptedQuestions));
        attemptedData.add(new PieChart.Data(String.format("Not Attempted (%d)",this.notAttemptedQuestions),this.notAttemptedQuestions));

        ObservableList<PieChart.Data> scoreChartData = this.correctAnswersChart.getData();
        scoreChartData.add(new PieChart.Data(String.format("Correct Answers (%d)",this.numberOfCorrectAnswers),this.numberOfCorrectAnswers));
        scoreChartData.add(new PieChart.Data(String.format("Incorrect Answers (%d)", this.numberOfIncorrectAnswers), this.numberOfIncorrectAnswers));
    }

    private void renderQuestions()
    {
        for(Question question : userAnswers.keySet())
        {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/student/QuestionResult.fxml"));
            try {
                Parent root = loader.load();
                QuestionResultController controller = loader.getController();
                controller.setValues(question,userAnswers.get(question));
                questionResultsContainer.getChildren().add(root);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
