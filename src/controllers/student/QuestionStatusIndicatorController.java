package controllers.student;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

import java.net.URL;
import java.util.ResourceBundle;

public class QuestionStatusIndicatorController implements Initializable {

    @FXML
    private Label questionNum;
    @FXML
    private Circle statusIndicator;

    private String selectedAnswer;

    public void setSelectedAnswer(String selectedAnswer) {
        this.selectedAnswer = selectedAnswer;
    }

    public String getSelectedAnswer() {
        return selectedAnswer;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setQuestionNum(String questionNum) {
        this.questionNum.setText(questionNum);
    }

    public void setDefaultColor()
    {
        statusIndicator.setFill(Color.web("#eee"));
        questionNum.setTextFill(Color.web("#0c0c0c"));
    }

    public void setCurrentQuestionColor()
    {
        statusIndicator.setFill(Color.web("#23C4ED"));
        questionNum.setTextFill(Color.web("#fff"));
    }


    public void setAttemptedColor()
    {
        statusIndicator.setFill(Color.web("#ccc"));
        questionNum.setTextFill(Color.web("#fff"));
    }

}
