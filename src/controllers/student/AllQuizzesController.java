package controllers.student;

import com.jfoenix.controls.JFXButton;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.layout.StackPane;
import listeners.NewScreenListener;
import models.Student;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AllQuizzesController implements Initializable {

    @FXML
    private StackPane stackPane;

    @FXML
    private JFXButton backBtn;

    private Student student;

    public void setStudent(Student student) {
        this.student = student;
        renderQuizList();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }


    private void addScreeenToStackPane(Node node)
    {
        Platform.runLater(() -> stackPane.getChildren().add(node));
    }

    private void renderQuizList()
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/student/QuizList.fxml"));

        try {
            Node node = (Node) loader.load();
            QuizListController controller = loader.getController();
            Platform.runLater(() -> stackPane.getChildren().add(node));
            controller.setStudent(this.student);
            controller.setScreenListener(new NewScreenListener() {
                private boolean flag = true;
                @Override
                public void changeScreen(Node node) {
                    addScreeenToStackPane(node);
                    backBtn.setDisable(flag);
                    flag = true;
                }

                @Override
                public void removeTopScreen() {
                    stackPane.getChildren().remove(stackPane.getChildren().size() - 1);
                    flag = false;
                    backBtn.setDisable(flag);
                }

                @Override
                public void handle(Event event) {

                }
            });
            controller.setCards();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void back()
    {
        ObservableList<Node> childrens = this.stackPane.getChildren();

        if(childrens.size() == 1)
            return;

        childrens.remove(childrens.size() - 1);
    }


}
