package controllers.student;

import javafx.application.Platform;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXRadioButton;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.FlowPane;
import listeners.NewScreenListener;
import models.Question;
import models.Quiz;
import models.QuizResult;
import models.Student;
import observables.QuestionObservable;
import org.controlsfx.control.Notifications;

public class QuestionScreenController implements Initializable {
    @FXML
    private Label title;
    @FXML
    private Label time;
    @FXML
    private Label question;
    @FXML
    private JFXRadioButton option1;
    @FXML
    private ToggleGroup options;
    @FXML
    private JFXRadioButton option2;
    @FXML
    private JFXRadioButton option3;
    @FXML
    private JFXRadioButton option4;
    @FXML
    private JFXButton previousBtn;
    @FXML
    private JFXButton nextBtn;
    @FXML
    private JFXButton finishQuizBtn;
    @FXML
    private FlowPane questionsStatusIndicationContainer;


    private Quiz quiz;
    private Student student;


    private Question currentQuestion;
    private List<Question> questionList;
    private Integer currentIndex = -1;
    private Integer rightAnswersCount = 0;

    private Map<Question,String> selectedAnswers;

    private QuestionObservable questionsObservable;

    // Timer fields
    private long minutes,seconds,hours,totalSeconds = 0;
    private Timer timer;

    // Screen Listener for adding and removing screens
    private NewScreenListener screenListener;

    public void setScreenListener(NewScreenListener screenListener) {
        this.screenListener = screenListener;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
        this.title.setText(this.quiz.getTitle());

        this.questionList = quiz.getQuestions();

        for(Question question : questionList)
            selectedAnswers.put(question, "");

        this.getData();
    }

    private void getData()
    {
        if(quiz != null)
        {
            Collections.shuffle(questionList);
            renderQuestionStatusIndicators();
            setNextQuestion();
            setTimer();
            disablePreviousBtn(); // Bcoz we're on 1st question by default

            options.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
                @Override
                public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                    JFXRadioButton selectedOption  = (JFXRadioButton)newValue;

                    if(selectedOption != null)
                    {
                        selectedAnswers.put(currentQuestion,selectedOption.getText());
                        QuestionStatusIndicatorController controller = (QuestionStatusIndicatorController)questionsStatusIndicationContainer.getChildren().get(currentIndex).getUserData();
                        controller.setSelectedAnswer(selectedOption.getText());
                    }
                }
            });
        }
    }

    @FXML
    public void nextQuestion(ActionEvent event)
    {
        Node questionStatusIndicator = this.questionsStatusIndicationContainer.getChildren().get(currentIndex);
        QuestionStatusIndicatorController controller = (QuestionStatusIndicatorController) questionStatusIndicator.getUserData();

        if(controller.getSelectedAnswer() != null)
            controller.setAttemptedColor();
        else
            controller.setDefaultColor();

        this.setNextQuestion();
    }

    private void setNextQuestion()
    {
        if(currentIndex < questionList.size())
        {
            ++currentIndex;

            // Changing the color
            Node questionStatusIndicator = this.questionsStatusIndicationContainer.getChildren().get(currentIndex);
            QuestionStatusIndicatorController controller = (QuestionStatusIndicatorController) questionStatusIndicator.getUserData();

            controller.setCurrentQuestionColor();

            enablePreviousQuestionBtn();
            this.currentQuestion = questionList.get(currentIndex);

            if(currentIndex >= questionList.size() - 1)
                disableNextQuestionBtn();

            setQuestion();

            if(!selectedAnswers.get(this.currentQuestion).equals(""))
                setSelectedAnswer(controller);
            else
                options.selectToggle(null);
        }
        else
            disableNextQuestionBtn();
    }

    private void setSelectedAnswer(QuestionStatusIndicatorController controller)
    {
        if(this.currentQuestion.getOption1().equals(controller.getSelectedAnswer()))
            options.selectToggle(option1);
        else if(this.currentQuestion.getOption2().equals(controller.getSelectedAnswer()))
            options.selectToggle(option2);
        else if(this.currentQuestion.getOption3().equals(controller.getSelectedAnswer()))
            options.selectToggle(option3);
        else
            options.selectToggle(option4);
    }

    private void setQuestion()
    {
//         this.question.setText(this.currentQuestion.getQuestion());

        List<String> options = new ArrayList<>();
        options.add(this.currentQuestion.getOption1());
        options.add(this.currentQuestion.getOption2());
        options.add(this.currentQuestion.getOption3());
        options.add(this.currentQuestion.getOption4());

        Collections.shuffle(options);


        // We are setting these options bcoz we've shuffled the options. otherwise we've binded our GUI fields with questionsObservable so we don't need those settings if we dont options to be shuffled up
        this.currentQuestion.setOption1(options.get(0));
        this.currentQuestion.setOption2(options.get(1));
        this.currentQuestion.setOption3(options.get(2));
        this.currentQuestion.setOption4(options.get(3));

        this.questionsObservable.setQuestion(this.currentQuestion);
    }

    @FXML
    public void previousQuestion(ActionEvent event)
    {
        Node questionStatusIndicator = this.questionsStatusIndicationContainer.getChildren().get(currentIndex);
        QuestionStatusIndicatorController controller = (QuestionStatusIndicatorController) questionStatusIndicator.getUserData();

        if(controller.getSelectedAnswer() != null)
            controller.setAttemptedColor();
        else
            controller.setDefaultColor();

        this.setPreviousQuestion();
    }

    private void setPreviousQuestion()
    {
        if(currentIndex > 0)
        {
            showNextQuestionBtn();
            --currentIndex;

            // Changing the color
            Node questionStatusIndicator = this.questionsStatusIndicationContainer.getChildren().get(currentIndex);
            QuestionStatusIndicatorController controller = (QuestionStatusIndicatorController) questionStatusIndicator.getUserData();
            controller.setCurrentQuestionColor();

            if(currentIndex <= 0)
                disablePreviousBtn();

            this.currentQuestion = questionList.get(currentIndex);
            setQuestion();

            if(!selectedAnswers.get(this.currentQuestion).equals(""))
                setSelectedAnswer(controller);
            else
                options.selectToggle(null);
        }
        else
            disablePreviousBtn();
    }

    private void bindFields()
    {
        // Binding our Observable fields with UI fields
        // We'll change the values in our observable class and it will get reflected in UI fields
        this.question.textProperty().bind(this.questionsObservable.questionProperty());
        this.option1.textProperty().bind(this.questionsObservable.option1Property());
        this.option2.textProperty().bind(this.questionsObservable.option2Property());
        this.option3.textProperty().bind(this.questionsObservable.option3Property());
        this.option4.textProperty().bind(this.questionsObservable.option4Property());
    }


    private void disableNextQuestionBtn()
    {
        this.nextBtn.setDisable(true);
    }

    private void showNextQuestionBtn()
    {
        this.nextBtn.setDisable(false);
    }

    private void disablePreviousBtn()
    {
        this.previousBtn.setDisable(true);
    }

    private void enablePreviousQuestionBtn()
    {
        this.previousBtn.setDisable(false);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.questionsObservable  = new QuestionObservable();
        bindFields();
        selectedAnswers = new HashMap<>();

    }

    private void convertTime()
    {
        minutes = TimeUnit.SECONDS.toMinutes(totalSeconds);
        seconds = totalSeconds - (minutes * 60);
        hours = TimeUnit.MINUTES.toHours(minutes);
        minutes = minutes - (hours * 60);

        totalSeconds--;
        time.setText(format(hours) + " : " + format(minutes) + " : " + format(seconds));

    }

    private String format(long value)
    {
        if(value < 10)
            return 0 +"" + value;

        return value + "";
    }

    private void setTimer()
    {
        totalSeconds = this.questionList.size() * 30;
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    convertTime();
                    if(totalSeconds <= 0)
                    {
                        timer.cancel();
                        time.setText("00 : 00 : 00");
                        finishQuiz();
                        Notifications.create().title("Error").text("Time up").position(Pos.CENTER).showError();
                    }
                });
            }
        };

        timer.schedule(timerTask,0,1000);
    }

    private void renderQuestionStatusIndicators()
    {
        for(int i = 0; i < this.questionList.size(); ++i)
        {
            try {
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/student/QuestionStatusIndicator.fxml"));
                Node node = (Node) loader.load();
                questionsStatusIndicationContainer.getChildren().add(node);
                QuestionStatusIndicatorController controller = loader.getController();
                controller.setQuestionNum(i + 1 + "");
                controller.setDefaultColor();

            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void finishQuiz()
    {
        System.out.println(this.selectedAnswers);
        QuizResult result = new QuizResult(this.rightAnswersCount,this.quiz,this.student);
        boolean res = result.save(selectedAnswers);

        if(res)
        {
            Notifications.create().title("Success").text("You Have Successfully Attempted this Quiz").position(Pos.CENTER).showInformation();
            this.timer.cancel();
            showResults();
        }
        else
            Notifications.create().title("Failed").text("Something Went Wrong").position(Pos.CENTER).showError();
    }

    private void showResults()
    {
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/student/QuizResult.fxml"));
            Parent node = loader.load();
            QuizResultController controller = loader.getController();
            controller.setValues(selectedAnswers,rightAnswersCount,quiz,questionList);
            this.screenListener.removeTopScreen();
            this.screenListener.changeScreen(node);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
