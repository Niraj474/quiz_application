package controllers.student;

import com.jfoenix.controls.JFXButton;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import listeners.NewScreenListener;
import models.Quiz;
import models.Student;

import java.net.URL;
import java.util.ResourceBundle;

public class QuizCardLayoutController implements Initializable {

    @FXML
    private Label title;
    @FXML
    private Label numOfQuestions;
    @FXML
    private JFXButton startQuizBtn;

    private NewScreenListener screenListener;

    private Quiz quiz;

    private Student student;

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
        this.title.setText(this.quiz.getTitle());
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public NewScreenListener getScreenListener() {
        return screenListener;
    }

    public void setTitle(String title)
    {
        this.title.setText(title);
    }

    public void setNumOfQuestions(String numOfQuestions)
    {
        this.numOfQuestions.setText(numOfQuestions);
    }

    @FXML
    public void startQuiz()
    {
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/student/QuestionScreen.fxml"));
            Parent node = loader.load();
            QuestionScreenController controller = loader.getController();
            controller.setQuiz(quiz);
            controller.setStudent(student);
            controller.setScreenListener(this.screenListener);
            this.screenListener.changeScreen(node);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void setScreenListener(NewScreenListener screenListener) {
        this.screenListener = screenListener;
    }
}
