package controllers.student;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.FlowPane;
import javafx.stage.Window;
import listeners.NewScreenListener;
import models.Quiz;
import models.Student;

import java.io.IOException;
import java.net.URL;
import java.util.*;

public class QuizListController implements Initializable {

    @FXML
    private FlowPane quizListContainer;

    private NewScreenListener screenListener;

    private Set<Quiz> keys;

    private List<Node> nodes;

    private Map<Quiz,Integer> quizzes;

    private Student student;

    public void setScreenListener(NewScreenListener screenListener) {
        this.screenListener = screenListener;

    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void setCards(){

        for(Quiz quiz : keys){
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().
                    getResource("/views/student/QuizCardLayout.fxml"));
            try {
                Node node = fxmlLoader.load();
                QuizCardLayoutController quizCardLayoutFXMLController = fxmlLoader.getController();
                quizCardLayoutFXMLController.setQuiz(quiz);
                quizCardLayoutFXMLController.setNumOfQuestions(quizzes.get(quiz) + "");
                quizCardLayoutFXMLController.setScreenListener(this.screenListener);
                quizCardLayoutFXMLController.setStudent(this.student);
                Platform.runLater(() -> quizListContainer.getChildren().add(node));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        quizzes = Quiz.getAllWithQuestionCount();
        keys = quizzes.keySet();

    }

}

