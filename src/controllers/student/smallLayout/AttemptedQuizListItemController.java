package controllers.student.smallLayout;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import listeners.AttemptedQuizListItemClickListener;
import models.Quiz;
import models.QuizResult;

import java.net.URL;
import java.util.ResourceBundle;



public class AttemptedQuizListItemController implements Initializable {


    @FXML
    private Label quizTitle;
    @FXML
    private VBox listItem;

    private QuizResult quizResult;
    private Quiz quiz;

    private AttemptedQuizListItemClickListener clickListener;

    public QuizResult getQuizResult() {
        return quizResult;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setBorder()
    {
        listItem.setBorder(new Border(new BorderStroke(Color.web("#12B0E8"),
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
    }

    public void removeBorder()
    {
        listItem.setBorder(new Border(new BorderStroke(Color.TRANSPARENT,
                BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
    }

    public void setData(Quiz quiz,QuizResult quizResult) {
        this.quiz = quiz;
        this.quizResult = quizResult;
        this.quizTitle.setText(this.quiz.getTitle());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setClickListener(AttemptedQuizListItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    @FXML
    public void fetchAttemptedQuizData(MouseEvent event)
    {
        Integer numberOfAttemptedQuestions = this.quizResult.getNumberOfAttemptedQuestions();
        Integer totalNumberOfQuestions = this.quiz.getNumberOfQuestions();
//        System.out.println(totalNumberOfQuestions + "  " + numberOfAttemptedQuestions);
        this.clickListener.itemClicked(totalNumberOfQuestions,numberOfAttemptedQuestions,this);
    }
}
