package controllers.student;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Tab;
import javafx.scene.input.MouseEvent;
import models.Student;

import java.net.URL;
import java.util.ResourceBundle;

public class MainScreenController implements Initializable {

    @FXML
    private Tab dashboard;
    @FXML
    private Tab allQuizzes;
    @FXML
    private Tab attemtedQuizzes;

    private Student student;

    public void setStudent(Student student)
    {
        this.student = student;
        setAllQuizzesScreen();
        setAttemptedQuizzesScreen();

    }

    private void setAllQuizzesScreen()
    {
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/student/AllQuizzes.fxml"));
            Parent node = loader.load();
            AllQuizzesController controller = loader.getController();
            controller.setStudent(this.student);

            this.allQuizzes.setContent(node);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setAttemptedQuizzesScreen()
    {
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/student/AttemptedQuizzesList.fxml"));
            Parent node = loader.load();
            AttemptedQuizzesListController controller = loader.getController();
            controller.setStudent(this.student);

            this.attemtedQuizzes.setContent(node);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    public void renderAttemptedQuizzes()
    {
        // re-rendering attempted quizzes if a student had given any quiz
        setAttemptedQuizzesScreen();
    }
}
