package controllers.admin;

import com.jfoenix.controls.JFXTabPane;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Tab;

import java.io.IOException;

public class AdminHomePageController {
    @FXML
    private JFXTabPane adminTabPane;
    @FXML
    private Tab addQuizTab;
    @FXML
    private Tab addStudentTab;

    public void initialize(){


        try {
            Parent quizTab = FXMLLoader.load(getClass().getResource("/views/admin/AddQuiz.fxml"));
            addQuizTab.setContent(quizTab);

            Parent studentTab = FXMLLoader.load(getClass().getResource("/views/admin/AddStudent.fxml"));
            addStudentTab.setContent(studentTab);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
