package controllers.admin;

import com.jfoenix.controls.*;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.stage.Window;
import javafx.util.Duration;
import models.Question;
import models.Quiz;
import org.controlsfx.control.Notifications;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AddQuizController {
    @FXML
    private JFXTreeView treeView;
    @FXML
    private JFXTextField quizTitletxt;
    @FXML
    private JFXTextArea questionTxt;
    @FXML
    private JFXTextField option1Txt;
    @FXML
    private JFXTextField option2Txt;
    @FXML
    private JFXTextField option3Txt;
    @FXML
    private JFXTextField option4Txt;
    @FXML
    private JFXRadioButton option1;
    @FXML
    private JFXRadioButton option2;
    @FXML
    private JFXRadioButton option3;
    @FXML
    private JFXRadioButton option4;
    @FXML
    private JFXButton addQuestionBtn;
    @FXML
    private JFXButton submitQuizBtn;
    @FXML
    private JFXButton setQuizTitleBtn;

    private ToggleGroup optionGroup;

    // Custom Variables
    private Quiz quiz = null;
    private List<Question> questions = new LinkedList<>();


    public void initialize(){
        optionsSetup();
        addQuestionBtn.tooltipProperty().set(new Tooltip("Add Current Question"));
        renderTreeView();
    }

    private void renderTreeView()
    {
        Map<Quiz,List<Question>> quizzes = Quiz.getAll();
        Set<Quiz> keys = quizzes.keySet();

        TreeItem root = new TreeItem("Quizzes");

        for(Quiz quiz : keys)
        {
            TreeItem quizTreeItem = new TreeItem(quiz);

            List<Question> questions = quizzes.get(quiz);

            for(Question question : questions)
            {
                TreeItem questionTreeItem = new TreeItem(question);
                questionTreeItem.getChildren().add(new TreeItem<>("Option A  :  " + question.getOption1()));
                questionTreeItem.getChildren().add(new TreeItem<>("Option B  :  " + question.getOption2()));
                questionTreeItem.getChildren().add(new TreeItem<>("Option C  :  " + question.getOption3()));
                questionTreeItem.getChildren().add(new TreeItem<>("Option D  :  " + question.getOption4()));
                questionTreeItem.getChildren().add(new TreeItem<>("Answer  :  " + question.getAnswer()));
                quizTreeItem.getChildren().add(questionTreeItem);

                quizTreeItem.setExpanded(true);
            }

            root.getChildren().add(quizTreeItem);
        }
        root.setExpanded(true);
        treeView.setRoot(root);
    }

    private void optionsSetup(){

        optionGroup = new ToggleGroup();
        option1.setToggleGroup(optionGroup);
        option2.setToggleGroup(optionGroup);
        option3.setToggleGroup(optionGroup);
        option4.setToggleGroup(optionGroup);

    }

    public boolean setQuizTitle(){
        String title = quizTitletxt.getText();

        if(title.trim().isEmpty())
        {
            // notifying the user that the title is empty
            Notifications notification = Notifications.create().title("Quiz Title").text("Invalid Title");
            notification.position(Pos.CENTER);
            notification.hideAfter(Duration.millis(2000));
            notification.showError();
            return false;
        }
        else
        {
            // Confirmation Dialog for Saving the title
            if(quiz == null){
                Window window = addQuestionBtn.getScene().getWindow();
                Dialog<ButtonType> dialog = new Dialog<>();
                dialog.setTitle("Quiz Title");
                dialog.initOwner(window);
                dialog.setContentText("After Submitting title, you want be able to modify it!");
                dialog.getDialogPane().getButtonTypes().add(ButtonType.OK);
                dialog.getDialogPane().getButtonTypes().add(ButtonType.CANCEL);

                dialog.showAndWait().ifPresent(result-> {
                    if(!result.getButtonData().isCancelButton())
                    {
                        quizTitletxt.setEditable(false);
                        this.quiz = new Quiz(title);
                    }

                });
            }
            return true;
        }

    }

    private boolean validateFields(){
        if(!setQuizTitle())
            return false;

        String question = this.questionTxt.getText();

        String option1 = this.option1Txt.getText();
        String option2 = this.option2Txt.getText();
        String option3 = this.option3Txt.getText();
        String option4 = this.option4Txt.getText();

        Toggle selectedOption = optionGroup.getSelectedToggle();

        Notifications notification;

        if(question.trim().isEmpty() || option1.trim().isEmpty()
                || option2.trim().isEmpty() || option3.trim().isEmpty() || option4.trim().isEmpty())
        {
            notification = Notifications.create().title("").text("Question and all the options should be filled properly").position(Pos.CENTER);
            notification.showError();
            return false;

        }else if(selectedOption == null){
            notification = Notifications.create().title("").text("Select a correct answer for the question").position(Pos.CENTER);
            notification.showError();
            return false;
        }
        else
            return true;
    }

    public void addNextQuestion(){
        addQuestion();
    }

    private boolean addQuestion()
    {
        boolean isValid = validateFields();
        if(isValid){
            // Save current Question and clearing up the fields for next question
            Question question = new Question();

            question.setQuestion(questionTxt.getText().trim());
            question.setOption1(option1Txt.getText().trim());
            question.setOption2(option2Txt.getText().trim());
            question.setOption3(option3Txt.getText().trim());
            question.setOption4(option4Txt.getText().trim());



            Toggle selected = optionGroup.getSelectedToggle();
            String ans = null;

            if(selected == option1)
                ans = option1Txt.getText().trim();
            else if(selected == option2)
                ans = option2Txt.getText().trim();
            else if(selected == option3)
                ans = option3Txt.getText().trim();
            else if(selected == option4)
                ans = option4Txt.getText().trim();

            question.setAnswer(ans);

            question.setQuiz(quiz);

            questions.add(question);


            // clearing the fields
            clearFields();


        }
        return isValid;
    }

    private void clearFields(){
        questionTxt.clear();
        option1Txt.clear();
        option2Txt.clear();
        option3Txt.clear();
        option4Txt.clear();
        if(optionGroup.getSelectedToggle() != null)
            optionGroup.getSelectedToggle().setSelected(false);
    }

    public void submitQuiz(){

        boolean flag;

        Window window = addQuestionBtn.getScene().getWindow();
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.setTitle("Question Submission");
        dialog.initOwner(window);
        dialog.setContentText("Do u want to Add this Current Question?");
        dialog.getDialogPane().getButtonTypes().add(ButtonType.YES);
        dialog.getDialogPane().getButtonTypes().add(ButtonType.NO);

        dialog.showAndWait().ifPresent(result-> {
            if(result.getButtonData().equals(ButtonType.NO))
                addQuiz(true);
            else
                addQuiz(addQuestion());

        });
    }

    private void addQuiz(boolean flag)
    {
        if(!flag)
            return;
        if(questions.isEmpty())
            Notifications.create().title("Quiz should contain atleast one question").position(Pos.CENTER).showError();
        else{
            if(flag)
            {
                quizTitletxt.setEditable(true);
                quizTitletxt.clear();
                if(quiz.save(questions))
                    Notifications.create().title("Success").text("Quiz Added Successfully").position(Pos.CENTER).showInformation();
                else
                    Notifications.create().title("Failed").text("Quiz Not Saved. Please Try Again").position(Pos.CENTER).showError();
            }
        }
    }

    public void cancelQuiz(){

    }
}
