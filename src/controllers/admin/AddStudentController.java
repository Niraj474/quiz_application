package controllers.admin;

import com.jfoenix.controls.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import models.Student;
import org.controlsfx.control.Notifications;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

public class AddStudentController implements Initializable {
    @FXML
    private VBox formContainer;
    @FXML
    private JFXTextField firstNameTxt;
    @FXML
    private JFXTextField lastNameTxt;
    @FXML
    private JFXTextField contactNumberTxt;
    @FXML
    private JFXTextField emailTxt;
    @FXML
    private JFXPasswordField passwordTxt;
    @FXML
    private JFXRadioButton male;
    @FXML
    private JFXRadioButton female;
    @FXML
    private JFXButton saveBtn;
    @FXML
    private TableView<Student> studentTable;
    @FXML
    private TableColumn<Student,String> studentIdColumn;
    @FXML
    private TableColumn<Student,String> emailColumn;
    @FXML
    private TableColumn<Student,String> firstNameColumn;
    @FXML
    private TableColumn<Student,String> lastNameColumn;
    @FXML
    private TableColumn<Student,String> contactNoColumn;
    @FXML
    private TableColumn<Student,Character> genderColumn;

//    NON-FXML Variables

    private ToggleGroup toggleGroup; // for gender

    public AddStudentController() {
        // FXML fields are initialized after completion of constructor
//        therefore we can't access any FXML variable inside constructor
//        To Overcome this problem, interface Initializable is provided which provides method known as initialize where we can access FXML Variables.
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initAll();
        radioBtnSetup();
        renderTable();
        this.contactNumberTxt.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue,
                                String newValue) {
                if (!newValue.matches("\\d*")) {
                    contactNumberTxt.setText(newValue.replaceAll("[^\\d]", ""));
                }
            }
        });
    }

    private void renderTable()
    {
        List<Student> students = Student.getAll();

        studentTable.getItems().clear();

        this.studentIdColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.emailColumn.setCellValueFactory(new PropertyValueFactory<>("email"));
        this.firstNameColumn.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        this.lastNameColumn.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        this.contactNoColumn.setCellValueFactory(new PropertyValueFactory<>("contact"));
        this.genderColumn.setCellValueFactory(new PropertyValueFactory<>("gender"));

        this.studentTable.getItems().addAll(students);
    }

    @FXML
    public void resetFields()
    {
        clearAllFields();
    }

    private void radioBtnSetup()
    {
        this.male.setSelected(true);
        this.male.setToggleGroup(toggleGroup);
        this.female.setToggleGroup(toggleGroup);
    }

    private void initAll()
    {
        toggleGroup = new ToggleGroup();
    }

    private String validate(Student student)
    {

        String message = null;

        Pattern pattern =  Pattern.compile("^[A-Za-z0-9+_.-]+@(.+)$");

        if(student.getFirstName().length() < 4)
            message = "First Name should have atleast 4 characters";
        else if(student.getLastName().length() < 2)
            message = "Last Name should have atleast 2 characters";
        else if(student.getContact().length() < 10)
            message = "Contact Number should have 10 digits";
        else if(!(pattern.matcher(student.getEmail()).matches()))
            message = "Enter a Valid Email";
        else if(student.getPassword().length() < 6)
            message = "Password must be more than 6 characters";

        return message;
    }

    @FXML
    public void saveStudent(ActionEvent e)
    {
        String firstName = this.firstNameTxt.getText();

        String lastName = this.lastNameTxt.getText();

        String contact = this.contactNumberTxt.getText();

        String email = this.emailTxt.getText();

        char[] password = this.passwordTxt.getText().toCharArray();

        Character genderTxt = 'M';

        JFXRadioButton gender = (JFXRadioButton) this.toggleGroup.getSelectedToggle();

        if(gender != null)
        {
            if(gender == this.female)
                genderTxt = 'F';
        }

        // Save Student

        Student s = new Student(firstName,lastName,contact,genderTxt,email,password);

        String message = validate(s);

        if(message != null)
        {
            Notifications.create().title("Invalid Details").text(message).position(Pos.CENTER).showError();
            return;
        }

        if(s.isExisting())
        {
            Notifications.create().title("Registration Failed").text("Email Already Exists").position(Pos.CENTER).showError();
            return;
        }

         s.save();

        if(s != null)
        {
            Notifications.create().title("Success").text("Student Successfully Registered").position(Pos.CENTER).showInformation();
            clearAllFields();
            studentTable.getItems().add(s);
        }
        else
            Notifications.create().title("Failed").text("Failed to register student").position(Pos.CENTER).showError();
    }


    private void clearAllFields()
    {
        this.firstNameTxt.clear();
        this.lastNameTxt.clear();
        this.contactNumberTxt.clear();
        this.emailTxt.clear();
        this.passwordTxt.clear();
        this.male.setSelected(true);
    }
}

